class Book:

    def __init__(self, book_author, book_title):
        self.book_author = book_author
        self.book_title = book_title


    def get_author(self):
        return (f"Author: {self.book_author}")

    def get_title(self):
        return (f"Title: {self.book_title}")


book = Book("Natalie Zina Walschots", "Hench")

print(book.get_author())
print(book.get_title())
