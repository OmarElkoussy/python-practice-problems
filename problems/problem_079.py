class ReceiptItem:
#
    def __init__(self, quantity, price):
        self.quantity = quantity
        self.price = price
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
    def get_total(self):
        return self.quantity * self.price
#    * get_total()          # Returns the quantity * price
#
# Example:
item = ReceiptItem(10, 3.45)
#
print(item.get_total())    # Prints 34.5
