class Employee:
#
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def get_fullname(self):
        return (f"{self.first_name} {self.last_name}")
    def get_email(self):
        return (f"{self.first_name.lower()}.{self.last_name.lower()}@company.com")
#    * get_email:    should return "«first name».«last name»@company.com"
#                    all in lowercase letters
#
# Example:
#    employee = Employee("Duska", "Ruzicka")
employee = Employee("Duska", "Ruzicka")
print(employee.get_fullname())
print(employee.get_email())
