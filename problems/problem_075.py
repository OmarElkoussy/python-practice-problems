class BankAccount:
    # Name:       BankAccount

# Required state:
#    * opening balance, the amount of money in the bank account
    def __init__(self, balance):
        self.balance = balance

# Behavior:
#    * get_balance()      # Returns how much is in the bank account
    def get_balance(self):
        return self.balance
    
#    * deposit(amount)    # Adds money to the current balance
    def deposit(self, amount):
        self.balance += amount
    
#    * withdraw(amount)   # Reduces the current balance by amount
    def withdraw(self, amount):
        self.balance -= amount
# Example:
account = BankAccount(100)
#
print(account.get_balance())  # prints 100
account.withdraw(50)
print(account.get_balance())  # prints 50
account.deposit(120)
print(account.get_balance())  # prints 170
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
