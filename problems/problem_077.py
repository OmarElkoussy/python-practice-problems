import math

class Circle:

    def __init__(self, radius):
        if radius < 0:
            raise ValueError
        self.radius = radius
#    * radius, a non-negative value
#
    def calculate_perimeter(self):
        
        return 2 * math.pi * self.radius

#    * calculate_perimeter()  # Returns the length of the perimater of the circle

    def calculate_area(self):
        return math.pi * pow(self.radius, 2)

#    * calculate_area()       # Returns the area of the circle
#
# Example:
circle = Circle(10)
#
print(circle.calculate_perimeter())
print(circle.calculate_area())
#
# There is pseudocode for you to guide you.



# class Circle
    # method initializer with radius
        # if radius is less than 0
            # raise ValueError
        # self.radius = radius

    # method calculate_perimeter(self)
        # returns 2 * math.pi * self.radius

    # method calculate_area(self)
        # returns math.pi * (self.radius squared)
