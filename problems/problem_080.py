from problem_079 import ReceiptItem

class Receipt: 
#
    def __init__(self, tax_rate):

        self.tax_rate = tax_rate

        self.items = []


    def add_item(self, item):

        self.items.append(item)


    def sub_total(self):
        sum = 0
        for item in self.items:
            sum += item.get_total()
        return sum

    
    def get_total(self):
        return self.get_subtotal() * (1 + self.tax_rate)
    



item = Receipt(.1)
item.add_item(ReceiptItem(4, 2.50))
item.add_item(ReceiptItem(2, 5.00))


print(item.get_subtotal())    
print(item.get_total())        


# class Receipt
    # method initializer with tax rate
        # self.tax_rate = tax_rate
        # self.items = new empty list

    # method add_item(self, item)
        # append item to self.items list

    # method get_subtotal(self)
        # sum = 0
        # for each item in self.items
            # increase sum by item.get_total()
        # return sum

    # method get_total(self)
        # return self.get_subtotal() * (1 + self.tax_rate)
