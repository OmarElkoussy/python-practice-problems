class Animal:

    def __init__(self, legs, color):
        self.legs = legs
        self.color = color

    def describe(self):                                     
        return (                                            
            self.__class__.__name__                         
            + " has "                                       
            + str(self.legs)                      
            + " legs and is primarily "                     
            + self.color                            
        )                      
                

class Dog(Animal):
    def speak(self):
        return "Bark!"
    

class Cat(Animal):
    def speak(self):
        return "Miao!"


class Snake(Animal):
    def speak(self):
        return "Ssssss!"



animal = Animal
dog = Dog("4", "Brown")
cat = Cat("4", "Black")
snake = Snake("0", "Green")

print(dog.describe())
print(dog.speak())
print(cat.describe())
print(cat.speak())
print(snake.describe())
print(snake.speak())




# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"
