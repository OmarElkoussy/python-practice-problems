class Book:

#
    def __init__(self, name, title):
        self.name = name
        self.title = title

    def get_author(self):
        return f"Author: {self.name}"
#    * get_author: should return "Author: «author name»"
    def get_title(self):
        return f"Title: {self.title}"
#    * get_title:  should return "Title: «title»"

book = Book("Natalie Zina Walschots", "Hench")

print(book.get_author())  # prints "Author: Natalie Zina Walschots"
print(book.get_title())   # prints "Title: Hench"
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
