# Write a class that meets these requirements.
class Student:

    def __init__(self, name):
        self.name = name
        self.scores = []
# Name:       Student
#
# Required state:
#    * name, a string
#
    def add_score(self, score):
        self.scores.append(score)
#    * add_score(score)   # Adds a score to their list of scores

    def get_average(self):
        if len(self.scores) == 0:
            return None
        
        return sum(self.scores) / len(self.scores)
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
student = Student("Omar")
print(student.get_average())
student.add_score(90)
print(student.get_average())
student.add_score(99)
student.add_score(87)
print(student.get_average())
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
